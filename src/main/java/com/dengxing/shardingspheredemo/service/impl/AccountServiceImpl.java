package com.dengxing.shardingspheredemo.service.impl;

import com.dengxing.shardingspheredemo.mapper.AccountMapper;
import com.dengxing.shardingspheredemo.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class AccountServiceImpl implements AccountService {

    @Autowired
    private AccountMapper accountMapper;


    @Override
    public void changeIn() {
        if (true) {
            throw new RuntimeException();
        }
        accountMapper.changeIn(2, 10);
    }

    @Override
//    @Transactional(propagation = Propagation.NESTED)
    public void changeOut() {
        accountMapper.changeIn(1, -10);
    }

    /**
     *
     */
    @Override
//    @Transactional
    public void noPublicTrans() {
//        accountMapper.changeIn(1, -10);
//        try {
//            noTrans1();
//        }catch (Exception e){
//            System.out.println("出错了");
//        }
        noTrans3();
    }

    /**
     *  自身调用导致事务失效
     */
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void noTrans1() {

        accountMapper.changeIn(2, 10);
        if (true){
            throw new RuntimeException();
        }
    }
    /**
     *   NOT_SUPPORTED 不支持事务
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void noTrans2() {

        accountMapper.changeIn(2, 10);
        if (true){
            throw new RuntimeException();
        }
    }

    /**
     *   非public 不支持事务
     */
    @Transactional(propagation = Propagation.REQUIRED)
    void noTrans3() {

        accountMapper.changeIn(2, 10);
        if (true){
            throw new RuntimeException();
        }
    }
}
