package com.dengxing.shardingspheredemo.service.impl;

import com.dengxing.shardingspheredemo.mapper.AccountMapper;
import com.dengxing.shardingspheredemo.mapper.UserMapper;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;


public class AccountNoService {


//    @Autowired
//    private AccountMapper accountMapper;


    /**
     * 没有被 Spring 管理
     * @throws FileNotFoundException
     */
//    @Transactional
    public void changeIn() throws FileNotFoundException {


        String mybatis = "/Users/dengxing/Desktop/code/ShardingSphereDemo/src/main/java/com/dengxing/shardingspheredemo/learning/mybatisdemo/mybatisDemo.xml";

        SqlSessionFactoryBuilder sqlSessionFactoryBuilder = new SqlSessionFactoryBuilder();
        InputStreamReader inputStreamReader = new InputStreamReader(new FileInputStream(mybatis));
        SqlSessionFactory sqlSessionFactory = sqlSessionFactoryBuilder.build(inputStreamReader);
        SqlSession sqlSession = sqlSessionFactory.openSession();
        UserMapper userMapper = sqlSession.getMapper(UserMapper.class);

       userMapper.insertOne();

       sqlSession.commit();

    }


}
