package com.dengxing.shardingspheredemo.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;
import java.io.Serializable;

/**
 * 秒杀库存表(Seckill)实体类
 *
 * @author makejava
 * @since 2021-07-06 14:40:11
 */
@Data
@TableName("Seckill")
public class Seckill implements Serializable {
    private static final long serialVersionUID = -72116572576424551L;
    /**
    * 商品库存ID
    */
    @TableId("seckill_id")
    private Long seckillId;
    /**
    * 商品名称
    */
    private String name;
    /**
    * 库存数量
    */
    private Integer number;
    /**
    * 秒杀开始时间
    */
    private Date startTime;
    /**
    * 秒杀结束时间
    */
    private Date endTime;
    /**
    * 创建时间
    */
    private Date createTime;

}