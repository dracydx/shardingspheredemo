package com.dengxing.shardingspheredemo.entity;


import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

@Data
@TableName("account")
public class Account implements Serializable {

    public Account() {
    }

    public Account(String name, int balance) {
        this.name = name;
        this.balance = balance;
    }

    private int id;

    private String name;

    private int balance;

}
