package com.dengxing.shardingspheredemo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @ClassName Course
 * @Description Course实体类
 * @Author dengxing
 * @Date 2021/6/29 6:16 PM
 */
@Data
@TableName("course")
public class Course {
    @TableId(value = "cid", type = IdType.AUTO)
    private Long cid;
    private String cname;
    private Long userId;
    private String status;
}
