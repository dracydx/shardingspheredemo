package com.dengxing.shardingspheredemo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @ClassName Order
 * @Description TODO
 * @Author dengxing
 * @Date 2021/7/2 3:18 PM
 */

@Data
@TableName("t_order")
public class Order {

    @TableId(value = "orderId", type = IdType.AUTO)
    private long orderId;

    private int userId;

    private String status;


}
