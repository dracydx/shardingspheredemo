package com.dengxing.shardingspheredemo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @ClassName Dict
 * @Description TODO
 * @Author dengxing
 * @Date 2021/7/2 9:49 AM
 */

@Data
@TableName("t_dict")
public class Dict {

    @TableId(value = "dict_Id", type = IdType.AUTO)
    private Long dictId;
    private String status;
    private String value;
}