package com.dengxing.shardingspheredemo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @ClassName User
 * @Description TODO
 * @Author dengxing
 * @Date 2021/7/2 9:25 AM
 */

@Data
@TableName("t_user")
public class User {

    @TableId(value = "userId", type = IdType.AUTO)
    private Long userId;
    private String username;
    private String status;

}
