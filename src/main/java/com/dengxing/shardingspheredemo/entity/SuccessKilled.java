package com.dengxing.shardingspheredemo.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import java.util.Date;
import java.io.Serializable;

/**
 * 秒杀成功明细表(SuccessKilled)实体类
 *
 * @author makejava
 * @since 2021-07-06 14:45:34
 */
@Data
@TableName("success_killed")
public class SuccessKilled implements Serializable {
    private static final long serialVersionUID = 631544918439065351L;
    /**
    * 秒杀商品ID
    */
    private Long seckillId;
    /**
    * 用户手机号
    */
    private Long userPhone;
    /**
    * 状态标识:-1:无效 0:成功 1:已付款 2:已发货
    */
    private Object state;
    /**
    * 创建时间
    */
    private Date createTime;

}