package com.dengxing.shardingspheredemo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dengxing.shardingspheredemo.entity.User;
import org.springframework.stereotype.Repository;

/**
 * @ClassName UserMapper
 * @Description TODO
 * @Author dengxing
 * @Date 2021/7/2 9:27 AM
 */
@Repository
public interface UserMapper extends BaseMapper<User> {

    User selectTopOne ();

    User selectAssociationOne ();

    void insertOne ();
}

