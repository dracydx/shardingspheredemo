package com.dengxing.shardingspheredemo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dengxing.shardingspheredemo.entity.Order;
import org.springframework.stereotype.Repository;

/**
 * @author dengxing
 */
@Repository
public interface OrderMapper extends BaseMapper<Order> {
}
