package com.dengxing.shardingspheredemo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dengxing.shardingspheredemo.entity.Course;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.stereotype.Repository;

/**
 * @author dengxing
 */
@Repository
//@MapperScan("com.dengxing.shardingspheredemo.mapper")
public interface CourseMapper extends BaseMapper<Course> {

}
