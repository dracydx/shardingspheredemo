package com.dengxing.shardingspheredemo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dengxing.shardingspheredemo.entity.Account;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

@Mapper
public interface AccountMapper extends BaseMapper<Account> {


    @Update("update account set balance = balance + #{amt} where id = #{id}")
    void changeIn(@Param("id") int id,@Param("amt") int amt);


    @Select("SELECT * FROM account WHERE id= #{id}")
    Account changeOut(@Param("id") int id);
}
