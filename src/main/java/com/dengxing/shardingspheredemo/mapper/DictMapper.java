package com.dengxing.shardingspheredemo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dengxing.shardingspheredemo.entity.Dict;
import org.springframework.stereotype.Repository;

/**
 * @ClassName DictMapper
 * @Description TODO
 * @Author dengxing
 * @Date 2021/7/2 9:49 AM
 */

@Repository
public interface DictMapper extends BaseMapper<Dict> {
}
