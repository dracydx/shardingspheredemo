package com.dengxing.shardingspheredemo.utils;

import okhttp3.Call;

/**
 * @ClassName ICallBack
 * @Description TODO
 * @Author dengxing
 * @Date 2021/6/28 10:31 AM
 */

public interface ICallBack {

    void onSuccessful(Call call, String data);

    void onFailure(Call call, String errorMsg);

}
