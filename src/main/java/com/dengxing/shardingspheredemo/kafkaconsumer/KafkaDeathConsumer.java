package com.dengxing.shardingspheredemo.kafkaconsumer;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.stereotype.Component;

@Component
public class KafkaDeathConsumer {

//    @KafkaListener(topics = {"kafka_receivedTopic_DLT"})
    public void death(String message, Acknowledgment acknowledgment) {
        try {
            System.out.println(message);
            int i = 2/0;
            acknowledgment.acknowledge();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
