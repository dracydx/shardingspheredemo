package com.dengxing.shardingspheredemo.learning.java8;

import com.google.common.base.Supplier;
import org.assertj.core.util.Lists;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @ClassName SupplierTest
 * @Description TODO
 * @Author dengxing
 * @Date 2021/7/12 4:08 PM
 */

public class SupplierTest {

    public static void main(String[] args) {
        //① 使用Supplier接口实现方法,只有一个get方法，无参数，返回一个值
        Supplier<Integer> supplier = new Supplier<Integer>() {
            @Override
            public Integer get() {
                //返回一个随机值
                return new Random().nextInt();
            }
        };

        System.out.println(supplier.get());

        System.out.println("********************");

        //② 使用lambda表达式，
        supplier = () -> new Random().nextInt();
        System.out.println(supplier.get());
        System.out.println("********************");

        //③ 使用方法引用
        Supplier<Double> supplier2 = Math::random;
        System.out.println(supplier2.get());

        final ArrayList<Dish> dishes = Lists.newArrayList(
                new Dish("pork", false, 800, Type.MEAT),
                new Dish("beef", false, 700, Type.MEAT),
                new Dish("chicken", false, 400, Type.MEAT),
                new Dish("french fries", true, 530, Type.OTHER),
                new Dish("rice", true, 350, Type.OTHER),
                new Dish("season fruit", true, 120, Type.OTHER),
                new Dish("pizza", true, 550, Type.OTHER),
                new Dish("prawns", false, 300, Type.FISH),
                new Dish("salmon", false, 450, Type.FISH)
        );

        IntSummaryStatistics collect = dishes.stream().collect(Collectors.summarizingInt(Dish::getCount));
        System.out.println(collect.getMax());
        System.out.println(collect.getCount());
        System.out.println(collect.getAverage());
        System.out.println(collect.getSum());

        //直接连接
        String join1 = dishes.stream().map(Dish::getName).collect(Collectors.joining());

        //逗号
        String join2 = dishes.stream().map(Dish::getName).collect(Collectors.joining(", "));
        System.out.println(join2);
        System.out.println(join1);



        // ① Supplier 接口可以理解为一个容器，用于装数据的。 ② Supplier 接口有一个 get 方法，可以返回值。
        List<String> collect1 = dishes.stream().map(Dish::getName).collect(Collectors.toList());
        collect1.forEach(System.out::println);


    }
}
