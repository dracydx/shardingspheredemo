package com.dengxing.shardingspheredemo.learning.java8;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @ClassName Dish
 * @Description TODO
 * @Author dengxing
 * @Date 2021/7/12 4:17 PM
 */
@AllArgsConstructor
@Data
public class Dish {


    private String name;

    private boolean d ;

    private int count;

    private Type category;


}

