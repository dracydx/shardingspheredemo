package com.dengxing.shardingspheredemo.learning.java8;

import com.baomidou.mybatisplus.autoconfigure.ConfigurationCustomizer;
import org.springframework.boot.autoconfigure.kafka.KafkaProperties;

import java.util.function.Consumer;

/**
 * @ClassName ConsumerTest
 * @Description TODO
 * @Author dengxing
 * @Date 2021/7/12 3:58 PM
 */

public class ConsumerTest {

    public static void main(String[] args) {


        Consumer consumer = (s) -> System.out.println(s);

        //  在上面的代码中，我们使用下面的 lambda 表达式作为 Consumer。
        //  仔细的看一下你会发现，lambda 表达式返回值就是一个 Consumer；
        //  所以，你也就能够理解为什么 forEach 方法可以使用 lamdda 表达式作为参数了吧
        Consumer<String> stringConsumer = new Consumer<String>() {
            @Override
            public void accept(final String s) {

            }
        };

        //  ① Consumer是一个接口，并且只要实现一个 accept 方法，就可以作为一个**“消费者”**输出信息。
        //  ② 其实，lambda 表达式、方法引用的返回值都是 Consumer 类型，所以，他们能够作为 forEach 方法的参数，并且输出一个值。


    }
}
