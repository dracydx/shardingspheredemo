package com.dengxing.shardingspheredemo.learning.java8;

/**
 * @author dengxing
 */

public enum Type {


    MEAT,
    OTHER,
    FISH;

}
