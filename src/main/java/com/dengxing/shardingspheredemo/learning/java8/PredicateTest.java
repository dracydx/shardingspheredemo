package com.dengxing.shardingspheredemo.learning.java8;

import com.google.common.base.Predicate;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @ClassName PredicateTest
 * @Description TODO
 * @Author dengxing
 * @Date 2021/7/12 4:52 PM
 */

public class PredicateTest {

    public static void main(String[] args) {



        //这段代码中，创建了一个 Predicate 接口对象，其中，实现类 test 方法，
        // 需要传入一个参数，并且返回一个 bool 值，所以这个接口作用就是判断！
        // 使用lambda表达式作为 predicate
        // ① Predicate 是一个谓词型接口，其实只是起到一个判断作用。 ② Predicate 通过实现一个 test 方法做判断。
        Stream<Integer> stream = Stream.of(1, 23, 3, 4, 5, 56, 6, 6);

        List<Integer> collect = stream.filter(s -> s > 4).collect(Collectors.toList());

        collect.forEach(System.out::println);


    }
}
