package com.dengxing.shardingspheredemo.learning.java8;

import java.util.function.Function;
import java.util.stream.Stream;

/**
 * @ClassName FunctionTest
 * @Description TODO
 * @Author dengxing
 * @Date 2021/7/12 4:56 PM
 */

public class FunctionTest {


    public static void main(String[] args) {

        // ① 使用map方法，泛型的第一个参数是转换前的类型，第二个是转化后的类型
        Function<String, Integer> function = s -> {
            return s.length();//获取每个字符串的长度，并且返回
        };


        //① Function 接口是一个功能型接口，是一个转换数据的作用。 ② Function 接口实现 apply 方法来做转换。
        //在 Function 接口的重要应用不得不说 Stream 类的 map 方法了，map 方法传入一个 Function 接口，返回一个转换后的 Stream类。


        Stream<String> stream = Stream.of("aaa", "bbbbb", "ccccccv");
        Stream<Integer> a = stream.map(String::length);
        a.forEach(System.out :: println);
    }

}