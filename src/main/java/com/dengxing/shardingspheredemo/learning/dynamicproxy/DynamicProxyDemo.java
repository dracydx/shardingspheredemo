package com.dengxing.shardingspheredemo.learning.dynamicproxy;

import java.lang.reflect.Proxy;

public class DynamicProxyDemo {


    public static void main(String[] args) {

        Person student = new Student();
        MyInvocationHandler myInvocationHandler = new MyInvocationHandler(student);
        Person person = (Person) Proxy.newProxyInstance(student.getClass().getClassLoader(),
                Student.class.getInterfaces(), myInvocationHandler);
        person.work();
        person.sleep();
    }
}
