package com.dengxing.shardingspheredemo.learning.dynamicproxy;

public class Student implements Person{
    @Override
    public void work() {
        System.out.println("读书");
        try {
            this.work2();
        } catch (Exception e) {

        }
    }

    @Override
    public void sleep() {
        System.out.println("睡觉");
    }


    public void work2() {
        System.out.println("不想读啊");
        int i = 1 / 0;
    }
}
