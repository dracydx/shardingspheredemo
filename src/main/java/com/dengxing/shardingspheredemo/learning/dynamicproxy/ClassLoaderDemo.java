package com.dengxing.shardingspheredemo.learning.dynamicproxy;

public class ClassLoaderDemo {

    /**
     * 1、BootstrapClassLoader 负责加载 JVM 运行时核心类，这些类位于 JAVA_HOME/lib/rt.jar 文件中，我们常用内置库 java.xxx.*
     * 都在里面，比如 java.util.*、java.io.*、java.nio.*、java.lang.* 等等。这个 ClassLoader 比较特殊，它是由 C 代码实现的，
     * 我们将它称之为「根加载器」。
     * <p>
     * 2、ExtensionClassLoader 负责加载 JVM 扩展类，比如 swing 系列、内置的 js 引擎、xml 解析器 等等，这些库名通常以 javax 开头，
     * 它们的 jar 包位于 JAVA_HOME/lib/ext/*.jar 中，有很多 jar 包。
     * <p>
     * 3、AppClassLoader 才是直接面向我们用户的加载器，它会加载 Classpath 环境变量里定义的路径中的 jar 包和目录。我们自己编写的代码以
     * 及使用的第三方 jar 包通常都是由它来加载的。
     *
     * @param args
     */
    public static void main(String[] args) {

//        URLClassLoader urlClassLoader = new URLClassLoader(new URL[]{},ClassLoader.getSystemClassLoader());

        ClassLoader classLoader = ClassLoader.getSystemClassLoader();

        try {
            Class<?> loadClass = classLoader.loadClass("com.dengxing.shardingspheredemo.learning.dynamicproxy.Student");
            Student student = (Student) loadClass.newInstance();
            student.sleep();

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

    }
}
