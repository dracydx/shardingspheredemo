package com.dengxing.shardingspheredemo.learning.dynamicproxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class MyInvocationHandler implements InvocationHandler {


    private Person person;


    public MyInvocationHandler(Person person) {
        this.person = person;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {

        if (method.getName().equals("sleep")) {
            System.out.println("先洗漱----再睡觉");
        }else {
            System.out.println("先吃饭----再看书");
        }


        method.invoke(person,args);

        return null;
    }
}
