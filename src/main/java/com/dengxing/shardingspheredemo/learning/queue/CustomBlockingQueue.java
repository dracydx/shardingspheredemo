package com.dengxing.shardingspheredemo.learning.queue;

import java.util.Random;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/**
 * CustomBlockingQueue
 * TODO
 * dengxing
 * 2021/7/13 11:35 AM
 * @author dengxing
 */

public class CustomBlockingQueue {

    public Object[] objects;
    /**
     * 读条件
     **/
    private Condition notEmpty;
    /**
     * 写条件
     **/
    private Condition notFull;
    /**
     * 出队下标
     */
    private int takeIndex;
    /**
     * 入队下标
     */
    private int putIndex;
    /**
     * 元素数量
     */
    private int size;
    /**
     * 锁对象
     */
    public ReentrantLock reentrantLock = new ReentrantLock();


    /**
     * 构造函数
     *
     * @param tabCount
     */
    public CustomBlockingQueue(int tabCount) {
        if (tabCount <= 0) {
            new NullPointerException();
        }
        objects = new Object[tabCount];
        notEmpty = reentrantLock.newCondition();
        notFull = reentrantLock.newCondition();
    }

    public boolean offer(Object object) {

        if (objects == null) {
            throw new NullPointerException();
        }
        reentrantLock.lock();
        try {
            while (size == objects.length) {
                System.out.println("队列已经满了");
                notFull.await();
            }
            objects[putIndex] = object;
            if (++putIndex == objects.length) {
                putIndex = 0;
            }

            size++;
            notEmpty.signal();
        } catch (Exception e) {
            notEmpty.signal();
            throw new NullPointerException();

        } finally {

            reentrantLock.unlock();

        }

        return false;


    }

    public Object take() {

        reentrantLock.lock();
        try {
            while (size == 0) {
                System.out.println("队列空了");
                //堵塞
                notEmpty.await();
            }
            Object obj = objects[takeIndex];
            //如果到了最后一个，则从头开始
            if (++takeIndex == objects.length) {
                takeIndex = 0;
            }
            size--;
            //唤醒写线程
            notFull.signal();
            return obj;
        } catch (Exception e) {
            //唤醒写线程
            notFull.signal();
        } finally {
            reentrantLock.unlock();
        }
        return null;
    }


    public static void main(String[] args) {

        Random random = new Random(100);
        CustomBlockingQueue customBlockingQueue = new CustomBlockingQueue(5);
        Thread thread1 = new Thread(() -> {
            for (int i = 0; i < 100; i++) {
                try {
                    Thread.sleep(300);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                customBlockingQueue.offer(i);
                System.out.println("生产者生产了：" + i);
            }
        });

        Thread thread2 = new Thread(() -> {
            for (int i = 0; i < 100; i++) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Object take = customBlockingQueue.take();
                System.out.println("消费者消费了：" + take);
            }
        });

        thread1.start();
        thread2.start();
    }
}
