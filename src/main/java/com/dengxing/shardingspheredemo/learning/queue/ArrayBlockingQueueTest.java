package com.dengxing.shardingspheredemo.learning.queue;

import java.util.concurrent.ArrayBlockingQueue;

/**
 * @ClassName ArrayBlockingQueueTest
 * @Description 一个由数组结构组成的有界阻塞队列。
 * 基于数组实现，队列容量固定。存/取数据的操作共用一把锁(默认非公平锁)，无法实现真正意义上存/取操作并行执行。
 * 由于基于数组，容量固定所以不容易出现内存占用率过高，但是如果容量太小，
 * 取数据比存数据的速度慢，那么会造成过多的线程进入阻塞(也可以使用offer()方法达到不阻塞线程)，
 * 此外由于存取共用一把锁，所以有高并发和吞吐量的要求情况下，我们也不建议使用ArrayBlockingQueue。
 * @Author dengxing
 * @Date 2021/7/15 4:45 PM
 */
public class ArrayBlockingQueueTest {

    public static void main(String[] args) {

        ArrayBlockingQueue<String> strings = new ArrayBlockingQueue<>(10);

        new Thread(()->{
            for (int i = 0; i < 100; i++) {
                try {
                    Thread.sleep(1000);
                    strings.put(String.valueOf(i));
                    System.out.println(strings.size());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();

        new Thread(()->{
           while (true){
               try {
                   Thread.sleep(0);
               } catch (InterruptedException e) {
                   e.printStackTrace();
               }
               try {
                   System.out.println(strings.take());
               } catch (InterruptedException e) {
                   e.printStackTrace();
               }

           }
        }).start();





    }
}
