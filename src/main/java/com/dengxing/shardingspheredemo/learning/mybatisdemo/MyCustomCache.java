package com.dengxing.shardingspheredemo.learning.mybatisdemo;

import com.alibaba.fastjson.JSON;
import org.apache.ibatis.cache.Cache;
import org.apache.ibatis.cache.CacheException;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisShardInfo;

import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * @ClassName MyCustomCache
 * @Description TODO
 * @Author dengxing
 * @Date 2021/7/16 11:42 AM
 */

public class MyCustomCache implements Cache {


    private final String id;
    private final ReadWriteLock readWriteLock = new ReentrantReadWriteLock(true);



    public MyCustomCache(final String id) {

        if (id == null) {
            throw new IllegalArgumentException("Cache instances require an ID");
        }
        this.id = id;
    }

    @Override
    public String getId() {
        return this.id;
    }

    @Override
    public void putObject(final Object o, final Object o1) {
        if (o1 != null) {
            JedisShardInfo jedisShardInfo = new JedisShardInfo("192.168.16.139",6379);
            jedisShardInfo.setPassword("dengxing");
            Jedis jedis = new Jedis(jedisShardInfo);
            jedis.set(o.toString(), JSON.toJSONString(o1));
        }
    }

    @Override
    public Object getObject(final Object o) {
        try {
            if (o != null) {

                JedisShardInfo jedisShardInfo = new JedisShardInfo("192.168.16.139",6379);
                jedisShardInfo.setPassword("dengxing");
                Jedis jedis = new Jedis(jedisShardInfo);
                String s = jedis.get(o.toString());
                return JSON.parse(s);
            }
        } catch (Exception e) {
            throw  e;
        }
        return null;
    }

    @Override
    public Object removeObject(final Object o) {
        JedisShardInfo jedisShardInfo = new JedisShardInfo("192.168.16.139",6379);
        jedisShardInfo.setPassword("dengxing");
        Jedis jedis = new Jedis(jedisShardInfo);
        jedis.del(o.toString());
        return null;
    }

    @Override
    public void clear() {
        JedisShardInfo jedisShardInfo = new JedisShardInfo("192.168.16.139",6379);
        jedisShardInfo.setPassword("dengxing");
        Jedis jedis = new Jedis(jedisShardInfo);
        jedis.flushAll();
    }

    @Override
    public int getSize() {

        return  0;
    }

    @Override
    public ReadWriteLock getReadWriteLock() {
        return readWriteLock;
    }

    @Override
    public boolean equals(Object o) {
        if (this.getId() == null) {
            throw new CacheException("Cache instances require an ID.");
        } else if (this == o) {
            return true;
        } else if (!(o instanceof Cache)) {
            return false;
        } else {
            Cache otherCache = (Cache)o;
            return this.getId().equals(otherCache.getId());
        }
    }

    @Override
    public int hashCode() {
        if (this.getId() == null) {
            throw new CacheException("Cache instances require an ID.");
        } else {
            return this.getId().hashCode();
        }
    }
}
