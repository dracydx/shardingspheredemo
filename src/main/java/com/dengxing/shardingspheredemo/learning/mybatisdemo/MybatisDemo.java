package com.dengxing.shardingspheredemo.learning.mybatisdemo;

import com.dengxing.shardingspheredemo.entity.User;
import com.dengxing.shardingspheredemo.mapper.UserMapper;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
 * @ClassName MybatisDemo
 * @Description TODO
 * @Author dengxing
 * @Date 2021/7/16 9:41 AM
 */

public class MybatisDemo {


    public static void main(String[] args) throws IOException {


        String resource = "/Users/dengxing/Downloads/ShardingSphereDemo/src/main/java/com/dengxing/shardingspheredemo/learning/mybatisdemo/mybatisDemo.xml";

        File file = new File(resource);
        FileReader reader = new FileReader(file);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(reader);

        SqlSession sqlSession = sqlSessionFactory.openSession();

        UserMapper mapper = sqlSession.getMapper(UserMapper.class);
        mapper.insertOne();
//        System.out.println(user.getUserId());


        SqlSession sqlSession1 = sqlSessionFactory.openSession();
        UserMapper mapper1 = sqlSession1.getMapper(UserMapper.class);


        User user1 = mapper1.selectTopOne();
        System.out.println(user1.getUserId());


        SqlSession sqlSession2 = sqlSessionFactory.openSession();
        UserMapper mapper2 = sqlSession2.getMapper(UserMapper.class);


        User user2 = mapper2.selectTopOne();
        System.out.println(user2.getUserId());
    }
}
