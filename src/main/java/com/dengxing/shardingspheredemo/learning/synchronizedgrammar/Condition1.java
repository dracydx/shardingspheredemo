package com.dengxing.shardingspheredemo.learning.synchronizedgrammar;

/**
 * @ClassName Condition2
 * @Description TODO
 * @Author dengxing
 * @Date 2021/6/28 11:07 AM
 */

public class Condition1 implements Runnable{


    /**
     * @Author dengxing
     * @Description  两个线程同时访问同一个对象的同步方法
     *               两个线程同时访问同一个对象的同步方法，是线程安全的。
     * @Date 11:03 AM 2021/6/28
     * @Param
     **/
    static Condition1 instance1 = new Condition1();

    @Override
    public void run() {
        method();
    }
    private synchronized void method() {
        System.out.println("线程名：" + Thread.currentThread().getName() + "，运行开始");
        try {
            Thread.sleep(4000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("线程：" + Thread.currentThread().getName() + "，运行结束");
    }
    public static void main(String[] args) {

        Thread thread1 = new Thread(instance1);
        Thread thread2 = new Thread(instance1);
        thread1.start();
        thread2.start();
        while (thread1.isAlive() || thread2.isAlive()) {
        }
        System.out.println("测试结束");
    }
}
