package com.dengxing.shardingspheredemo.learning.synchronizedgrammar;

/**
 * @ClassName Condition2
 * @Description TODO
 * @Author dengxing
 * @Date 2021/6/28 11:07 AM
 */

public class Condition3 implements Runnable{


    /**
     * @Author dengxing
     * @Description  两个线程同时访问（一个或两个）对象的静态同步方法
     *               两个线程同时访问（一个或两个）对象的静态同步方法，是线程安全的。
     * @Date 11:19 AM 2021/6/28
     * @Param
     **/
    static Condition3 instance1 = new Condition3();
    static Condition3 instance2 = new Condition3();

    @Override
    public void run() {
        method();
    }
    private synchronized static void method() {
        System.out.println("线程名：" + Thread.currentThread().getName() + "，运行开始");
        try {
            Thread.sleep(4000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("线程：" + Thread.currentThread().getName() + "，运行结束");
    }


    public static void main(String[] args) {

        Thread thread1 = new Thread(instance1);
        Thread thread2 = new Thread(instance2);
        thread1.start();
        thread2.start();
        while (thread1.isAlive() || thread2.isAlive()) {
        }
        System.out.println("测试结束");
    }
}
