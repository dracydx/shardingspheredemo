package com.dengxing.shardingspheredemo.learning.volatilegrammar;

/**
 * @ClassName VolatileMain
 * @Description TODO
 * @Author dengxing
 * @Date 2021/6/28 5:29 PM
 */

public class VolatileMain {

    public static void main(String[] args) {

        TestVolatile testVolatile = new TestVolatile();
        testVolatile.start();

        while (true) {
            if (testVolatile.isFlag()) {
                System.out.println("flag被改了");
                break;
            }
        }
    }
}
