package com.dengxing.shardingspheredemo.learning.volatilegrammar;

/**
 * @ClassName VolatileSingle
 * @Description TODO
 * @Author dengxing
 * @Date 2021/6/28 5:38 PM
 */

public class VolatileSingle {


    //instance = new Singleton(); // 第10行

    //// 可以分解为以下三个步骤
    //1 memory=allocate();// 分配内存 相当于c的malloc
    //2 ctorInstance(memory) //初始化对象
    //3 s=memory //设置s指向刚分配的地址


    //// 上述三个步骤可能会被重排序为 1-3-2，也就是：
    //1 memory=allocate();// 分配内存 相当于c的malloc
    //3 s=memory //设置s指向刚分配的地址
    //2 ctorInstance(memory) //初始化对象


    // 而一旦假设发生了这样的重排序，
    // 比如线程A在第10行执行了步骤1和步骤3，
    // 但是步骤2还没有执行完。这个时候线程B执行到了第7行
    // ，它会判定instance不为空，
    // 然后直接返回了一个未初始化完成的instance！


    private volatile static VolatileSingle instance;

    public VolatileSingle getInstance() {
        if (instance == null) {

            synchronized (VolatileSingle.class) {

                if (instance == null) {

                    instance = new VolatileSingle();
                }
            }
        }
        return instance;
    }


    private VolatileSingle() {

    }
}
