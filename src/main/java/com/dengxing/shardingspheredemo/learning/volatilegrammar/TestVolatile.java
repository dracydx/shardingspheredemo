package com.dengxing.shardingspheredemo.learning.volatilegrammar;

/**
 * @ClassName TestVolatile
 * @Description TODO
 * @Author dengxing
 * @Date 2021/6/28 5:28 PM
 */

public class TestVolatile extends Thread {

    /**
     * @Author dengxing
     * @Description 内存可见性问题
     * @Date 5:39 PM 2021/6/28
     * @Param
     **/
    volatile boolean flag = false;

    @Override
    public void run() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        flag = true;
        System.out.println("flag:" + isFlag());
    }

    public boolean isFlag() {
        return flag;
    }
}
