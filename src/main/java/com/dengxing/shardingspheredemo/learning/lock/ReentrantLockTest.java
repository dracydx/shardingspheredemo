package com.dengxing.shardingspheredemo.learning.lock;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @ClassName ReentrantLockTest
 * @Description TODO
 * @Author dengxing
 * @Date 2021/7/13 2:21 PM
 */

public class ReentrantLockTest {

    private static final Lock lock = new ReentrantLock(false);

    public static void main(String[] args) {
        //1天有24个小时    1代表1天：将1天转化为小时
        System.out.println(TimeUnit.DAYS.toHours(1) + "小时");
        new Thread(() -> test(), "线程1").start();
        new Thread(() -> test(), "线程2").start();
    }

    public static void test() {

        try {
            lock.lock();
            System.out.println(Thread.currentThread().getName() + "获取了锁");
            TimeUnit.SECONDS.sleep(2);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            System.out.println(Thread.currentThread().getName() + "释放了锁");
            lock.unlock();
        }
    }
}
