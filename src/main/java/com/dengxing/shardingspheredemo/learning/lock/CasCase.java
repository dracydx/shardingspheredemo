package com.dengxing.shardingspheredemo.learning.lock;

import okhttp3.internal.Util;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @ClassName CasCase
 * @Description TODO
 * @Author dengxing
 * @Date 2021/7/13 2:31 PM
 */

public class CasCase implements Runnable {

    private  static final AtomicInteger value = new AtomicInteger(0);

    public static void main(String[] args) throws InterruptedException {

        ExecutorService executorService = new ThreadPoolExecutor(10, 2147483647,
                60L, TimeUnit.SECONDS, new SynchronousQueue<>(), Util.threadFactory("CasCase", false));

        int count = 100;
        for (int i = 0; i < count; i++) {
            executorService.submit(new CasCase());
        }
        Thread.sleep(1000);
        System.out.println(value);
        executorService.shutdown();
    }


    public void add() {
        value.incrementAndGet();
    }

    @Override
    public void run() {
        add();
    }
}
