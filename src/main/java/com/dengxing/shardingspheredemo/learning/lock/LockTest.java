package com.dengxing.shardingspheredemo.learning.lock;

import java.util.concurrent.locks.ReentrantLock;

/**
 * @ClassName LockTest
 * @Description TODO
 * @Author dengxing
 * @Date 2021/7/13 9:32 AM
 */

public class LockTest {


    public static void main(String[] args) {


        new Thread(new Runnable() {
            @Override
            public void run() {

                synchronized (this) {
                    System.out.println("第一次获取到锁" + this);
                    int index = 1;
                    while (true) {
                        synchronized (this) {
                            System.out.println("第 " + index + " 次获取到锁" + this);

                        }
                        if (index > 10) {
                            break;
                        }
                    }
                }
            }
        }).start();


        ReentrantLock reentrantLock = new ReentrantLock(false);



    }
}
