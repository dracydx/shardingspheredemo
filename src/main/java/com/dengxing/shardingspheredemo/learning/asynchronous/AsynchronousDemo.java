package com.dengxing.shardingspheredemo.learning.asynchronous;


import com.google.common.util.concurrent.*;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;

import java.util.*;
import java.util.concurrent.*;

/**
 * @ClassName AsynchronousDemo
 * @Description TODO
 * @Author dengxing
 * @Date 2021/7/8 2:38 PM
 */

public class AsynchronousDemo {


    public static void main(String[] args) throws ExecutionException, InterruptedException {


        ThreadFactory threadFactory = Executors.defaultThreadFactory();
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(4, Integer.MAX_VALUE, 5000,
                TimeUnit.SECONDS, new LinkedBlockingDeque<>(), threadFactory);

        System.out.println("task start");

        List<Future<String>> futures = new LinkedList<>();
        for (int i = 0; i < 1; i++) {
            futures.add(threadPoolExecutor.submit(new SayHello()));
            Future<?> submit = threadPoolExecutor.submit(new SayWorld());
        }

        Future<String> future = threadPoolExecutor.submit(new SayHello());
        Future<?> submit = threadPoolExecutor.submit(new SayWorld());
        String s = future.get();

        System.out.println("task end");


        ListeningExecutorService listeningExecutorService = MoreExecutors.listeningDecorator(threadPoolExecutor);

        ListenableFuture<String> listenableFuture = listeningExecutorService.submit(new SayHello());

        listenableFuture.addListener(() -> {
            try {
                System.out.println("任务完成" + listenableFuture.get());
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }, listeningExecutorService);


        Futures.addCallback(listenableFuture, new FutureCallback<String>() {
            @Override
            public void onSuccess(@NullableDecl final String s) {
                System.out.println("onSuccess" + s);
            }

            @Override
            public void onFailure(final Throwable throwable) {
                System.out.println("onFailure");
                throwable.printStackTrace();
            }
        }, listeningExecutorService);


        CompletableFuture<String> future1 = CompletableFuture.supplyAsync(() -> "hello");

        CompletableFuture<String> future2 = future1.thenApplyAsync(t -> t + " world");

        String s1 = future2.get();
        System.out.println("CompletableFuture : " + s1);


        CompletableFuture<String> future3 = future1.thenComposeAsync(t -> CompletableFuture.supplyAsync(() -> t + " world !"));

        String s2 = future3.get();
        System.out.println("thenComposeAsync" + s2);


        CompletableFuture<String> f1 = CompletableFuture.supplyAsync(() -> {
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            return "hello ";
        });

        CompletableFuture<String> f2 = CompletableFuture.supplyAsync(() -> {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return " world !!";
        });


        CompletableFuture<String> f3 = f1.thenCombine(f2, (t1, t2) ->
                t1 + t2
        );
        System.out.println("thenCombine" + f3.get());


        Random random = new Random();
        CompletableFuture<String> ff1 = CompletableFuture.supplyAsync(() -> {
            try {
                Thread.sleep(1000 + random.nextInt(1000));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return "hello";
        });
        CompletableFuture<String> ff2 = CompletableFuture.supplyAsync(() -> {
            try {
                Thread.sleep(1000 + random.nextInt(1000));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return "world";
        });

        CompletableFuture<String> ff3 = ff1.applyToEitherAsync(ff2, t -> t);
        String s3 = ff3.get();
        System.out.println("applyToEitherAsync " + s3);


        int i = Runtime.getRuntime().availableProcessors();
        System.out.println("processors available to the Java virtual machine " + i);


    }


}

class SayWorld implements Runnable {

    @Override
    public void run() {
        System.out.println("hello world ");
    }
}

class SayHello implements Callable<String> {

    @Override
    public String call() throws Exception {

        Thread.sleep(3000);
        System.out.println("i am callable !");
        return "hello world ";
    }
}

