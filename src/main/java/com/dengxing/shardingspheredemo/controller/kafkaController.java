package com.dengxing.shardingspheredemo.controller;


import com.alibaba.fastjson.JSON;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.concurrent.ExecutionException;

/**
 * 防止kafka 消息丢失
 * 1⃣  生产由于网络原因丢失 ： 通过 send().get() 来判断是否推送成功，设置 失败重试次数
 * 2⃣  消费者丢失 ： 消息准备进行真正消费的时候，突然挂掉了，消息实际上并没有被消费，但是 offset 却被自动提交了。
 * 手动关闭闭自动提交 offset，每次在真正消费完消息之后之后再自己手动提交 offset ，但是会带来消息重复消费的情况（消费完了，没来的及提交offset就挂了）
 * <p>
 * 3⃣  kafka 服务丢失，broker 挂了，副本还没来的及同步的消息丢失，设置 acks = all 、replication.factor >= 3、 min.insync.replicas > 1
 */
@RestController
public class kafkaController {


    @Autowired
    private KafkaTemplate<Object, Object> kafkaTemplate;


    @RequestMapping(value = "setKafka", method = RequestMethod.GET)
    public String setKafka() {

        ArrayList<String> strings = new ArrayList<>();


        LinkedList<String> strings1 = new LinkedList<>();


        System.out.println("");
        return "hello  world! ";
    }

    /**
     * 异步回调
     *
     * @param normalMessage
     */
    @GetMapping("/test/{message}")
    public void sendMessage(@PathVariable("message") String normalMessage) {
        ListenableFuture<SendResult<Object, Object>> send = kafkaTemplate.send("topic1", 0, "meityijia", normalMessage);

        send.addCallback(new ListenableFutureCallback<SendResult<Object, Object>>() {
            @Override
            public void onFailure(Throwable throwable) {
                System.out.println(throwable.getMessage());
            }

            @Override
            public void onSuccess(SendResult<Object, Object> result) {

                System.out.println(JSON.toJSONString(result));
            }
        });
    }

    /**
     * 同步方法
     *
     * @param normalMessage
     * @throws ExecutionException
     * @throws InterruptedException
     */
    @GetMapping("/test1/{message}")
    public void sendMessage1(@PathVariable("message") String normalMessage) throws ExecutionException, InterruptedException {

        SendResult<Object, Object> objectObjectSendResult = kafkaTemplate.send("topic1", 0, "meityijia", normalMessage).get();
        if (objectObjectSendResult.getProducerRecord() != null) {
            System.out.println("发送消息成功！");
        }


    }


}
