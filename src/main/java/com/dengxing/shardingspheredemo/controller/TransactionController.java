package com.dengxing.shardingspheredemo.controller;

import com.dengxing.shardingspheredemo.service.AccountService;
import com.dengxing.shardingspheredemo.service.impl.AccountNoService;
import com.dengxing.shardingspheredemo.service.impl.AccountServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.FileNotFoundException;


/**
 * author : dengxing
 * <p>
 * 原子性 （atomicity）:强调事务的不可分割.
 * 一致性 （consistency）:事务的执行的前后数据的完整性保持一致.
 * 隔离性 （isolation）:一个事务执行的过程中,不应该受到其他事务的干扰
 * 持久性（durability） :事务一旦结束,数据就持久到数据库
 * file : TransactionController
 */
@RestController
public class TransactionController {


    @Autowired
    private AccountService accountService;


    /**
     * Spring 事务  -------  事务的传播机制
     * 简单的理解就是多个事务方法相互调用时,事务如何在这些方法间传播。
     * 在Spring中对于事务的传播行为定义了七种类型分别是：REQUIRED、SUPPORTS、MANDATORY、REQUIRES_NEW、NOT_SUPPORTED、NEVER、NESTED。
     * REQUIRED(Spring默认的事务传播类型) : 如果当前没有事务，则自己新建一个事务，如果当前存在事务，则加入这个事务
     * SUPPORTS  :当前存在事务，则加入当前事务，如果当前没有事务，就以非事务方法执行
     * MANDATORY : 当前存在事务，则加入当前事务，如果当前事务不存在，则抛出异常。
     * REQUIRES_NEW : 创建一个新事务，如果存在当前事务，则挂起该事务。
     * NOT_SUPPORTED : 始终以非事务方式执行,如果当前存在事务，则挂起当前事务
     * NEVER : 不使用事务，如果当前事务存在，则抛出异常
     * NESTED : 如果当前事务存在，则在嵌套事务中执行，否则REQUIRED的操作一样（开启一个事务）
     * {
     * REQUIRED情况下，调用方存在事务时，则被调用方和调用方使用同一事务，那么被调用方出现异常时，由于共用一个事务，
     * 所以无论调用方是否catch其异常，事务都会回滚而在NESTED情况下，被调用方发生异常时，调用方可以catch其异常，
     * 这样只有子事务回滚，父事务不受影响
     * }
     *
     * @return
     */

    @RequestMapping(value = "transaction", method = RequestMethod.GET)
    @Transactional(propagation = Propagation.REQUIRED)
    public String transactionIndex() {
        try {
            accountService.changeOut();
        } catch (Exception e) {

        }

        accountService.changeIn();
        return "ok";
    }

    /**
     * 事务失效 demo
     * 1、 从 MySQL 5.5.5 开始的默认存储引擎是：InnoDB，之前默认的都是：MyISAM，所以这点要值得注意，底层引擎不支持事务再怎么搞都是白搭。
     * 2、 没有被 Spring 管理
     * 3、 方法不是public 修饰
     * 4、 因为它们发生了自身调用，就调该类自己的方法，而没有经过 Spring 的代理类，默认只有在外部调用事务才会生效，这也是老生常谈的经典问题了。
     *  (解决方案之一就是在的类中注入自己，用注入的对象再调用另外一个方法，这个不太优雅，)
     *
     * 5、数据源没有配置事务管理器
     * 6、不支持事务： Propagation.NOT_SUPPORTED
     * 7、 异常被吃了
     * 8、异常类型错误 ：这样事务也是不生效的，因为默认回滚的是：RuntimeException，如果你想触发其他异常的回滚，需要在注解上配置一下，
     *    如：@Transactional(rollbackFor = Exception.class) 这个配置仅限于 Throwable 异常类及其子类。
     * @return
     */

//    @Transactional(propagation = Propagation.REQUIRED)
    @RequestMapping(value = "transactionFailure", method = RequestMethod.GET)
    public String transactionFailure() throws FileNotFoundException {


        accountService.noPublicTrans();
        return "OK";
    }


}
