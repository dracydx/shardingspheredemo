package com.dengxing.shardingspheredemo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.sound.midi.Soundbank;
import java.util.HashMap;

/**
 * @ClassName HomeController
 * @Description TODO
 * @Author dengxing
 * @Date 2021/6/28 10:33 AM
 */
@RestController
public class HomeController {

    @Autowired
    private KafkaTemplate<Object,Object> kafkaTemplate;


    @RequestMapping(value = "index",method = RequestMethod.GET)
    public HashMap<String, String> index(){

        HashMap<String, String> meiyijia = new HashMap<>();
        meiyijia.put("300","daa");

        if (meiyijia.get("300") != null){

            System.out.println("daaa");
        }

        return meiyijia;
    }

    @RequestMapping(value = "kafka",method = RequestMethod.GET)
    public String setKafkaTemplate (){

        String defaultTopic = kafkaTemplate.getDefaultTopic();

        System.out.println(defaultTopic);

        ListenableFuture<SendResult<Object, Object>> send = kafkaTemplate.send("topicDemo", "I am demo");
        send.addCallback(new ListenableFutureCallback<SendResult<Object, Object>>() {
            @Override
            public void onFailure(Throwable throwable) {
                System.out.println("消费发送失败:" + throwable.getMessage());
            }

            @Override
            public void onSuccess(SendResult<Object, Object> result) {
                if (null != result.getRecordMetadata()) {
                    System.out.println("消费发送成功 offset:" + result.getRecordMetadata().offset());
                    return;
                }
                System.out.println("消息发送成功!1");
            }
        });


        return "ok";
    }

}
