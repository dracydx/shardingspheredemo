package com.dengxing.shardingspheredemo.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.listener.ConsumerAwareListenerErrorHandler;

@Configuration
public class CustomListenerErrorHandler {

    @Autowired
    private KafkaTemplate<Object, Object> kafkaTemplate;

    @Bean
    public ConsumerAwareListenerErrorHandler listenerErrorHandler() {
        return (message, exception, consumer) -> {
            System.out.println("--- 消费时发生异常 ---");
            kafkaTemplate.send("kafka_receivedTopic_DLT", message.getPayload());
            return message;
        };
    }
}
