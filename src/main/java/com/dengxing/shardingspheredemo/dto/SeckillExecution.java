package com.dengxing.shardingspheredemo.dto;

import com.dengxing.shardingspheredemo.entity.SuccessKilled;
import com.dengxing.shardingspheredemo.enums.SeckillStatEnum;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @ClassName SeckillExecution
 * @Description TODO
 * @Author dengxing
 * @Date 2021/7/6 3:09 PM
 */
@Data
@AllArgsConstructor
public class SeckillExecution {

    private long seckillId;

    //秒杀执行结果的状态
    private int state;

    //状态的明文标识
    private String stateInfo;

    //当秒杀成功时，需要传递秒杀成功的对象回去
    private SuccessKilled successKilled;

    public SeckillExecution(final long secKillId, SeckillStatEnum success, final SuccessKilled successKilled) {
        this.seckillId = secKillId;
        this.state = success.getState();
        this.successKilled = successKilled;
    }
}
