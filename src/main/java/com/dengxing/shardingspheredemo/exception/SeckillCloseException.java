package com.dengxing.shardingspheredemo.exception;

import com.dengxing.shardingspheredemo.dto.SeckillException;

/**
 * @ClassName SeckillCloseException
 * @Description TODO
 * @Author dengxing
 * @Date 2021/7/7 9:09 AM
 */

public class SeckillCloseException  extends SeckillException {
    public SeckillCloseException(final String seckill_data_rewrite) {
        super(seckill_data_rewrite);
    }
}
