package com.dengxing.shardingspheredemo;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.dengxing.shardingspheredemo.entity.User;
import com.dengxing.shardingspheredemo.mapper.UserMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @ClassName ShardingsphereUserTest
 * @Description TODO
 * @Author dengxing
 * @Date 2021/7/2 9:32 AM
 */

@RunWith(SpringRunner.class)
@SpringBootTest
public class ShardingsphereUserTest {

    @Autowired
    private UserMapper userMapper;

    @Test
    public void addUser() {

        User user = new User();
        user.setUsername("dengxing");
        user.setStatus(" normal ");
        userMapper.insert(user);

    }

    @Test
    public void getUser(){

        QueryWrapper<User> userQueryWrapper = new QueryWrapper<>();
        userQueryWrapper.eq("username","dengxing");
        User user = userMapper.selectOne(userQueryWrapper);
        System.out.println(user.getUserId());
        System.out.println(user.getUsername());
    }

}
