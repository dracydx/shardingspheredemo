package com.dengxing.shardingspheredemo;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.redisson.Redisson;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.concurrent.TimeUnit;

@SpringBootTest
@RunWith(SpringRunner.class)
public class RedissonTest {


    @Value("${spring.redis.host}")
    private String redisHost;
    @Value("${spring.redis.port}")
    private String redisPort;

    @Value("${spring.redis.password}")
    private String password;

    @Test
    public void RedissonTestLock() {

        Config config = new Config();
        String redisUrl = "redis://"+redisHost+":"+redisPort;
        config.useSingleServer().setAddress(redisUrl).setPassword(password);
        RedissonClient redissonClient = Redisson.create(config);
        RLock lock = redissonClient.getLock("LOCK-1");
        try {
            boolean tryLock = lock.tryLock(60, 50, TimeUnit.SECONDS);
            if(tryLock) {
                System.out.println("******************** Business ********************");
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            if(lock.isHeldByCurrentThread()) {   // 获取锁的线程才能解锁
                lock.unlock();
            } else {
                // 没有获得锁的处理
            }
        }

    }
}
