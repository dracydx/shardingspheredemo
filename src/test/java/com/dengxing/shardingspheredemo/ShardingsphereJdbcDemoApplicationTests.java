package com.dengxing.shardingspheredemo;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.dengxing.shardingspheredemo.entity.Course;
import com.dengxing.shardingspheredemo.mapper.CourseMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


/**
 * @ClassName ShardingsphereJdbcDemoApplicationTests
 * @Description TODO
 * @Author dengxing
 * @Date 2021/6/29 6:23 PM
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class ShardingsphereJdbcDemoApplicationTests {


    @Autowired
    private CourseMapper courseMapper;

    @Test
    public void addCourse() {
        Course course = new Course();
        course.setCname("Java");
        course.setUserId(100L);
        course.setStatus("Normal");
        courseMapper.insert(course);
    }


    @Test
    public void findCourse() {
        QueryWrapper<Course> wrapper = new QueryWrapper<>();
        wrapper.eq("cid", 536248443081850881L);
        Course course = courseMapper.selectOne(wrapper);
        System.out.println(course.getCname());
    }

    @Test
    public void addCourse1() {
        Course course = new Course();
        //cid由我们设置的策略，雪花算法进行生成
        course.setCname("python");
        //分库根据user_id
        course.setUserId(100L);
        course.setStatus("Normal");
        courseMapper.insert(course);

        course.setCname("c++");
        course.setUserId(111L);
        courseMapper.insert(course);
    }

    @Test
    public void findCourse1() {
//        QueryWrapper<Course> wrapper = new QueryWrapper<>();
//        wrapper.eq("cid", 536248443081850881L);
//        Course course = courseMapper.selectOne(wrapper);
//        System.out.println(course.getCname());

        QueryWrapper<Course> objectQueryWrapper = new QueryWrapper<>();
        objectQueryWrapper.eq("user_id", 100);
        Course course = courseMapper.selectOne(objectQueryWrapper);

    }
}

