package com.dengxing.shardingspheredemo;


import com.dengxing.shardingspheredemo.entity.Account;
import com.dengxing.shardingspheredemo.mapper.AccountMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import static org.springframework.transaction.annotation.Propagation.REQUIRED;

@SpringBootTest
@RunWith(SpringRunner.class)
public class AccountTest {

    @Autowired
    private AccountMapper accountMapper;

    @Test
    public void addAccount() {
        Account account = new Account();
        account.setName("tom");
        account.setBalance(100);
        accountMapper.insert(account);
    }

    @Test
    @Transactional(propagation = REQUIRED)
    public void transactionalTest() {

        Account account = accountMapper.changeOut(1);
        System.out.println(account.getBalance());

        test1();
        test2();

        System.out.println(111111);
    }

    public void test1() {
        accountMapper.changeIn(1, -10);
    }

    @Transactional(propagation = REQUIRED)
    public void test2() {
        accountMapper.changeIn(2, 10);
//        throw new RuntimeException();
    }

    @Test
    @Transactional

    public void transactionalTest1() {

//        Account account = accountMapper.changeOut(1);
//        System.out.println(account.getBalance());

        accountMapper.changeIn(1, -10);
        accountMapper.changeIn(2, 10);

//        System.out.println(111111);
    }



}
