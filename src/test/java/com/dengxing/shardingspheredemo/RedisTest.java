package com.dengxing.shardingspheredemo;

import com.dengxing.shardingspheredemo.entity.Account;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ZSetOperations;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.*;
import java.util.concurrent.TimeUnit;

@SpringBootTest
@RunWith(SpringRunner.class)
public class RedisTest {


    @Autowired
    private RedisTemplate<Object,Object> redisTemplate;

    @Test
    public void setRedisString() {
        redisTemplate.opsForValue().set("dengxing","I am dengxing");
    }


    @Test
    public void setRedisList() {
        ArrayList<Account> accounts = new ArrayList<>();
        accounts.add(new Account("dengxing",111));
        accounts.add(new Account("jack", 1000));
        redisTemplate.opsForValue().set("account", accounts);
    }

    @Test
    public void whileTrueSet() {

        int i = 0;
        while (true){

            String s = String.valueOf(i);
            System.out.println(s);
            redisTemplate.opsForValue().set(s, "test count");
            i++;

            if (i > 1000000){
                break;
            }
        }

    }

    @Test
    public void getRedisValue() {
        Object o = redisTemplate.opsForValue().get("1");
        System.out.println(o);

    }


    @Test
    public void fiveDataType() {
        /**
         * string
         */
        redisTemplate.boundValueOps("boundkey").set("boundvale");
        redisTemplate.boundValueOps("StringKey").set("StringValue",1, TimeUnit.MINUTES);

        HashOperations<Object, Object, Object> objectObjectObjectHashOperations = redisTemplate.opsForHash();
        objectObjectObjectHashOperations.put("dengxing","dengxing1","dengxing1");

        objectObjectObjectHashOperations.put("hash","hashkey","hashvalue");

        HashMap<String, String> stringStringHashMap = new HashMap<>();
        stringStringHashMap.put("hashmapkay","hashmapvalue");
        stringStringHashMap.put("hashmapkay1","hashmapvalue2");
        stringStringHashMap.put("hashmapkay3","hashmapvalue3");

        redisTemplate.boundHashOps("hashmap").putAll(stringStringHashMap);

        redisTemplate.boundSetOps("setKey").add("setvaule111","setvalue11","setvalue21");

        LinkedList<String> strings = new LinkedList<>();
        strings.add("1");
        strings.add("2");

        redisTemplate.boundListOps("list").leftPushAll("listvalue","listvalue1","listvalue");


        ZSetOperations<Object, Object> opsForZSet = redisTemplate.opsForZSet();
        opsForZSet.add("zsetkey","zsetvalue", 100);
        opsForZSet.add("zsetkey","zsetvalue1", 300);
        opsForZSet.add("zsetkey","zsetvalue2", 400);
        opsForZSet.incrementScore("zsetkey","zsetvalue", 100);

        Set<Object> zsetkey = redisTemplate.boundZSetOps("zsetkey").rangeByScore(0D, 1000.2D);


        assert zsetkey != null;
        zsetkey.forEach(System.out::println);



    }

    @Test
    public void getFiveDataType() {
        ArrayList<Object> strings = new ArrayList<>();
        strings.add("dengxing");
        strings.add("dengxing1");
        HashOperations<Object, Object, Object> objectObjectObjectHashOperations = redisTemplate.opsForHash();
        List<Object> objects = objectObjectObjectHashOperations.multiGet("dengxing", strings);
        objects.forEach(System.out::println);

    }
}
