package com.dengxing.shardingspheredemo;

import com.dengxing.shardingspheredemo.entity.Order;
import com.dengxing.shardingspheredemo.mapper.OrderMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @ClassName ShardingsphereJdbcOrderTest
 * @Description TODO
 * @Author dengxing
 * @Date 2021/7/2 3:21 PM
 */

@RunWith(SpringRunner.class)
@SpringBootTest
public class ShardingsphereJdbcOrderTest {


    @Autowired
    private OrderMapper orderMapper;

    @Test
    public void addOrder() {
        Order order = new Order();
        order.setStatus("normal");
        order.setOrderId(10);
        order.setUserId(10);
        orderMapper.insert(order);

    }

}
