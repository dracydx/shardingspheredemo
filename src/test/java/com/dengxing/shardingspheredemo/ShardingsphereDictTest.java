package com.dengxing.shardingspheredemo;

import com.dengxing.shardingspheredemo.entity.Dict;
import com.dengxing.shardingspheredemo.mapper.DictMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @ClassName ShardingsphereDictTest
 * @Description TODO
 * @Author dengxing
 * @Date 2021/7/2 9:50 AM
 */

@RunWith(SpringRunner.class)
@SpringBootTest
public class ShardingsphereDictTest {



    @Autowired
    private DictMapper dictMapper;

    @Test
    public void addDict(){

        Dict dict = new Dict();
        dict.setStatus("da");
        dict.setValue("normal");
        dictMapper.insert(dict);

    }

}
